<?php
$retourLigne = "\n";
$console = isset($argc);
$slash = "/";
$pipe = "|";
$bl = "\n";
if (!$console)
{
	print("<!DOCTYPE html><head><title>test</title></head><body>");
	$retourLigne = "<br></tr><tr>\n";
	$slash = "</td><td>";
	$pipe = "</td><td>";
	$bl = "<br>\n";
}

$connection = mysqli_connect("bts.bts-malraux72.net", "l.chevreul", "passe", "l.chevreul", "63330");
if(mysqli_connect_errno($connection))
{
	die("Connection impossible : ". mysqli_connect_error());
}
else
{

	$resultat = mysqli_query($connection, "SELECT * FROM Adherent;");
	if($resultat != false)
	{

		function afficher_ligne($slash, $pipe, $retourLigne, $tuple_a_afficher, $console, $afficher_en_tete = FALSE){
			print("\n");
			$i = 0;
			foreach($tuple_a_afficher as $key => $value)
			{
				print("<td>".($afficher_en_tete ? $key : $value)."</td>" . ($i == count($tuple_a_afficher)-1 ? $retourLigne :($i % 2 == 0 ? $slash : $pipe) ) );
				$i++;
			}
		}

		$nombre_tuples = mysqli_num_rows($resultat);
		print("la requete a retournee : ". $nombre_tuples. " tuple");
		print($nombre_tuples>1 ? "s":"");
		print($bl);
		if(!$console)
		{
				print("<table><tr>");
		}
		$num_ligne = 0;
		while($tuple = mysqli_fetch_assoc($resultat))
		{
			if($num_ligne % 5 == 0)
			{
				afficher_ligne($slash, $pipe, $retourLigne, $tuple, $console, TRUE);
			}
			afficher_ligne($slash, $pipe, $retourLigne, $tuple, $console);
			$num_ligne++;
		}
		if(is_resource($resultat))
		{
			mysqli_free_result($resultat);
		}
	}
	mysqli_close($connection);
}

if(!$console)
{
	print("</td></tr></table></body></html>");
}

?>
